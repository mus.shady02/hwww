﻿#include <iostream>
#include <string>

using namespace std;

class Animal
{
public:
	virtual void Voice() const = 0;
};

class dog : public Animal
{
	void Voice() const override  {
		cout << "Woof" << endl;
	}
};

class cat : public Animal
{
	void Voice() const override {
		cout << "meow" << endl;
	}
};

class cow : public Animal
{
	void Voice() const override  {
		cout << "moo" << endl;
	}
};

int main(){
	
	Animal* animals[3];
	animals[0] = new dog();
	animals[1] = new cat();
	animals[2] = new cow();

	for (Animal* a : animals) {
		a->Voice();
	}
	return 0;
}